#pragma once

#include <QString>
#include "RepoNode.h"

class Repo : public RepoNode
{
    public:
        Repo(QString pCloneUrl);

    private:
        QString mCloneUrl;
};
